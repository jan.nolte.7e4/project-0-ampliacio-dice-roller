package cat.itb.diceroller

import android.graphics.Color
import android.graphics.drawable.AnimationDrawable
import android.graphics.drawable.Drawable
import android.media.MediaPlayer
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.CountDownTimer
import android.view.View
import android.view.animation.AnimationUtils
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import com.google.android.material.snackbar.Snackbar
import java.util.*
import kotlin.random.Random
import kotlin.concurrent.schedule

class MainActivity : AppCompatActivity() {
    lateinit var rollDiceButton: Button
    lateinit var resetButton: Button
    lateinit var imageDice1: ImageView
    lateinit var imageDice2: ImageView
    lateinit var mediaPlayer: MediaPlayer
    val vectorDaus = intArrayOf(R.drawable.empty_dice, R.drawable.dice_1, R.drawable.dice_2, R.drawable.dice_3, R.drawable.dice_4, R.drawable.dice_5, R.drawable.dice_6)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mediaPlayer= MediaPlayer.create(this, R.raw.rollingdice)
        setContentView(R.layout.activity_main)
        rollDiceButton = findViewById(R.id.roll_button)
        resetButton = findViewById(R.id.reset_button)
        imageDice1 = findViewById(R.id.dau1)
        imageDice2 = findViewById(R.id.dau2)
        var dice1 :Int
        var dice2 :Int
        rollDiceButton.setOnClickListener{
            playSound(mediaPlayer)
            dice1 = Random.nextInt(1,7)
            dice2 = Random.nextInt(1,7)
            rollDice(imageDice1, dice1)
            rollDice(imageDice2, dice2)
            jackpotMessage(dice1, dice2)
            resetButton.visibility = View.VISIBLE
            resetButton.setOnClickListener {
                mediaPlayer.seekTo(0)
                imageDice1.setImageResource(vectorDaus[0])
                imageDice2.setImageResource(vectorDaus[0])
                resetButton.visibility = View.GONE
            }
            imageDice1.setOnClickListener {
                dice1 = Random.nextInt(1,7)
                playSound(mediaPlayer)
                rollDice(imageDice1, dice1)
                jackpotMessage(dice1, dice2)
            }
            imageDice2.setOnClickListener {
                dice2 = Random.nextInt(1,7)
                playSound(mediaPlayer)
                rollDice(imageDice2, dice2)
                jackpotMessage(dice1, dice2)
            }
        }
    }
    fun jackpotMessage(dice1: Int, dice2: Int){
        if(dice1==dice2 && dice1==6){
            val snack = Snackbar.make(findViewById(R.id.top_coordinator), "JACKPOT!", Snackbar.LENGTH_SHORT)
            snack.show()
        }
    }
    fun rollDice(iV:ImageView, diceV:Int){
        val animationZoomIn = AnimationUtils.loadAnimation(this, R.anim.dice_animation)
        iV.startAnimation(animationZoomIn)
        Timer().schedule(1000){
            iV.setImageResource(vectorDaus[diceV])
        }
    }
    fun playSound(mp:MediaPlayer){
        mp.seekTo(0)
        mp.start()
    }
}